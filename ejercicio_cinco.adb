procedure Ejercicio_Cinco is
N: constant Positive := 100;
K: constant Positive := 8;

type Indice_Usu is new Positive range 1 .. N;

type Indice_Proc is new Positive range 1 .. K;

type Error is new Positive range 1 .. 3;

type Programa is new Integer;

type Linea is new Integer;

function Ejecucion(Prog: in Programa; L: in Linea) return Error is
begin
	return 1;
end;

function Linea_Siguiente(Prog: in Programa) return Linea is
begin
	return 0;
end;

task Administrador is
	entry ObtenerProcesador(Procesador: out Indice_Proc);
	entry ProcesadorLiberado(Procesador: in Indice_Proc);
end;

task type Usuario_Tipo is
	entry ResultadoEjecucion(Res: in Error; L: in Linea);
	entry AsignarID(ID: in Indice_Usu);
end;

task type Procesador_Tipo is
	entry CargarPrograma(Prog: in Programa; Usuario: in Indice_Usu);
end;

Procesadores: array (Indice_Proc) of Procesador_Tipo;
Usuarios: array (Indice_Usu) of Usuario_Tipo;

task body Administrador is
Carga_Procesadores: array (Indice_Proc) of Integer := (others => 0);
Min: Integer := Integer'Last;
begin
	loop
		select 
			accept ObtenerProcesador(Procesador: out Indice_Proc) do
				for I in Carga_Procesadores'Range loop
					if Carga_Procesadores(I) < Min then
						Min := Carga_Procesadores(I);
						Procesador := I;
					end if;
				end loop;
			end ObtenerProcesador;
		or
			accept ProcesadorLiberado(Procesador: in Indice_Proc) do
				Carga_Procesadores(Procesador) := Carga_Procesadores(Procesador) - 1;
			end;
		end select;
	end loop;
end;

task body Procesador_Tipo is
Programas: array (Indice_Usu) of Programa := (others => -1);
Resultado: Error;
I: Indice_Usu := Indice_Usu'First;
L: Linea;
begin
	loop
		select 
			accept CargarPrograma(Prog: in Programa; Usuario: in Indice_Usu) do
				Programas(Usuario) := Prog;
			end;
		else
			if Programas(I) /= -1 then 
				L := Linea_Siguiente(Programas(I));
				Resultado := Ejecucion(Programas(I), L);
				if Resultado /= 2 then
					Programas(I) := -1;
					Usuarios(I).ResultadoEjecucion(Resultado, L);
				end if;
				I := I + 1;
			end if;
		end select;
	end loop;
end;

task body Usuario_Tipo is
Procesador: Indice_Proc;
Prog: Programa;
Terminado: Boolean := False;
Resultado: Error;
U: Indice_Usu;
begin
	-- Es necesario usar un select con un unico accept sin else? (no compila sin else!)
	accept AsignarID(ID: in Indice_Usu) do 
		U := ID;
	end;

	Administrador.ObtenerProcesador(Procesador);
	Procesadores(Procesador).CargarPrograma(Prog, U);
	while not Terminado loop
		accept ResultadoEjecucion(Res: in Error; L: in Linea) do
			Resultado := Res;
		end;
		Administrador.ProcesadorLiberado(Procesador);
		if Resultado = 1 then
			-- Arreglo el programa sabiendo la linea del error
			Administrador.ObtenerProcesador(Procesador);
			Procesadores(Procesador).CargarPrograma(Prog, U);
		elsif Resultado = 3 then
			Terminado := True;
		end if;
	end loop;
	
end;

begin
	for I in Indice_Usu loop
		Usuarios(I).AsignarID(I);
	end loop;
end;