procedure Ejercicio_Cuatro is

-- Declaraciones
task Central is
	entry Uno;
	entry Dos;
	entry Dos_Timeout;
end;

task Timer is
	entry Reset(T: in Standard.Duration);
end;

task Proceso_Uno;

task Proceso_Dos;

--Definiciones
task body Timer is
Tiempo: Standard.Duration;
begin
	accept Reset(T: in Standard.Duration) do
		Tiempo := T;
	end;
	delay(Tiempo);
	Central.Dos_Timeout;
end;

task body Central is
Solo_Dos: Boolean := False;
begin	
	loop
		select
			when (not Solo_Dos) => accept Uno;
		or
			accept Dos do
				if not Solo_Dos then
					Timer.Reset(3.0*60.0);
					Solo_Dos := True;
				end if;
			end;
		or
			accept Dos_Timeout do
				Solo_Dos := False;
			end;
		end select;
	end loop;
end;

task body Proceso_Uno is
begin
	loop
		select 
			Central.Uno;
		or
			delay(2.0*60.0);
		end select;
	end loop;
end;

task body Proceso_Dos is
begin
	loop
		select 
			Central.Dos;
		else
			Delay(60.0);
		end select;
	end loop;
end;

begin
	Null;
end;