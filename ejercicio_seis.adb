procedure Ejercicio_Seis is
E: constant Integer := 10;
P: constant Integer := 10;
Capacidad_Consultorio: constant Integer := 10;

type Nota is new Integer;

task Medico is
	entry AtenderPersona;
	entry AtenderEnfermera;
end;

task Consultorio is
	entry DejarNota(N: in Nota);
	entry TomarNota(N: out Nota);
end;

task body Medico is
N: Nota;
begin
	loop
		select
			when (AtenderPersona'Count = 0) =>
				accept AtenderEnfermera;
		or
				accept AtenderPersona;
		else 
			select 
				Consultorio.TomarNota(N);
			else
				null;
			end select;
		end select;
	end loop;
end;

task body Consultorio is
Buffer: array (1 .. Capacidad_Consultorio) of Nota;
Ultimo: Integer := 0;
begin
	loop
		select
				when (Ultimo < Capacidad_Consultorio) =>
					accept DejarNota(N: in Nota) do
						Ultimo := Ultimo + 1;
						Buffer(Ultimo) := N;
					end;
			or
				when (Ultimo > 0) =>
					accept TomarNota(N: out Nota) do
						N := Buffer(Ultimo);
						Ultimo := Ultimo - 1;
					end;
			end select;
	end loop;
end;

task type Persona_Tipo;

task type Enfermera_Tipo;

task body Persona_Tipo is
Intentos: Integer := 0;
Atendida: Boolean := False;
begin
	while not Atendida and Intentos <= 3 loop
		select 
			Medico.AtenderPersona;
			Atendida := True;
		or delay(5.0*60.0);
			Intentos := Intentos + 1;
			delay(10.0*60.0);
		end select;
	end loop;
end;

task body Enfermera_Tipo is
N: Nota;
begin
	select
		Medico.AtenderEnfermera;
	else
		Consultorio.DejarNota(N);
	end select;
end;

begin
	null;
end;