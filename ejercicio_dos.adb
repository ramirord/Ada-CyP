procedure Ejercicio_Dos is

task Banco is
	entry  Entrar;
end;

task type Cliente;

task body Banco is
begin
	loop
		accept Entrar do
			delay(3.0);
		end;
	end loop;
end;

task body Cliente is
begin
	select
		Banco.Entrar;
	or
		delay(10.0*60.0);
	end select;
end;

clientes: array (Integer range  1 .. 10) of Cliente;

begin
	null;
end;