with Ada.Text_IO; 
with Ada.Numerics.Float_Random;
use Ada;
use Ada.Numerics.Float_Random;

procedure Ejercicio_Uno is
-- Relacionado a generación de números
subtype Time is Float;

-- Constantes
CARGA_AUTO: constant Integer := 2;
CARGA_CAMIONETA: constant Integer := 3;
CARGA_CAMION: constant Integer := 6;

procedure Imprimir_Carga(c: in Integer) is
package Integer_IO is new Text_IO.Integer_IO (Integer);
begin
	Text_IO.Put("Carga actual es:");
	Integer_IO.Put(c);
	Text_IO.Put_Line("");
end;

task Puente is
	entry EntrarAuto;
	entry EntrarCamioneta;
	entry EntrarCamion;
	entry Salir(Peso: in Integer);
end;

task Auto;
task Camioneta;
task Camion;

task body Puente is
CARGA_MAXIMA: constant Integer := 6;
Carga: Integer := 0;
begin
	loop
		select 
			when (Carga + CARGA_AUTO <= CARGA_MAXIMA)
				=> accept EntrarAuto do
				Carga := Carga + CARGA_AUTO;
				Text_IO.Put_Line("Auto entró");
				Imprimir_Carga(Carga);
			end;
		or
			when (Carga + CARGA_CAMIONETA <= CARGA_MAXIMA)
				=> accept EntrarCamioneta do
				Carga := Carga + CARGA_CAMIONETA;
				Text_IO.Put_Line("Camioneta entró");
				Imprimir_Carga(Carga);
			end;
		or
			when (Carga + CARGA_CAMION <= CARGA_MAXIMA)
			=> accept EntrarCamion do
				Carga := Carga + CARGA_CAMION;
				Text_IO.Put_Line("Camión entró");
				Imprimir_Carga(Carga);
			end;
		or
			accept Salir(Peso: in Integer) do
				Carga := Carga - Peso;
				
				Imprimir_Carga(Carga);
			end;
		end select;
	end loop;
end;

task body Auto is
Gen: Numerics.Float_Random.Generator;
begin
	Numerics.Float_Random.Reset(Gen);
	loop
		delay(Random(Gen));
		Puente.EntrarAuto;
		delay(2.0);
		Text_IO.Put_Line("Auto salió");
		Puente.Salir(CARGA_AUTO);
	end loop;
end;

task body Camioneta is
begin
	loop
		delay(2.3234);
		Puente.EntrarCamioneta;
		delay(2.0);
		Text_IO.Put_Line("Camioneta salió");
		Puente.Salir(CARGA_CAMIONETA);
	end loop;
end;

task body Camion is
begin
	loop
		delay(4.0);
		Puente.EntrarCamion;
		delay(2.0);
		Text_IO.Put_Line("Camión salió");
		Puente.Salir(CARGA_CAMION);
	end loop;
end;

begin
	null;
end;