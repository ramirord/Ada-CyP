with Ada.Text_IO;

procedure Ejercicio_Tres is
N: constant Integer := 5000;
M: constant Integer := 40000;

task type Contador;

task Administrador is
    entry IniciarBusqueda(Item: out Integer);
    entry ResultadoBusqueda(Count: in Integer);
end;

task body Contador is
Num: Integer;
Cant: Integer := 0;
Vector: array (Integer range 1 .. M) of Integer;
begin
    Administrador.IniciarBusqueda(Num);

    for I in Vector'Range loop
        Vector(I) := I;
    end loop;

    for I in Vector'Range loop
        if Vector(I) = Num then
            Cant := Cant + 1;
        end if;
    end loop;

    Administrador.ResultadoBusqueda(Cant);
end;

Contadores: array (Integer range 1 .. N) of Contador;

task body Administrador is
Numero: constant Integer := 5;
Cuenta_Total: Integer := 0;
begin
    for I in 1 .. 2*Contadores'Length loop
        select
            accept IniciarBusqueda(Item: out Integer) do
                Item := Numero;
            end;
        or
            accept ResultadoBusqueda(Count: in Integer) do
                Cuenta_Total := Cuenta_Total + Count;
            end;
        end select;
    end loop;
    Ada.Text_IO.Put_Line(Integer'Image(Cuenta_Total));
end;

begin
	null;
end;